(function (angular) {

    // Create all modules and define dependencies to make sure they exist
    // and are loaded in the correct order to satisfy dependency injection
    // before all nested files are concatenated by Gulp

    // Config
    angular.module('provaAngularComponent.config', [])
        .value('provaAngularComponent.config', {
            debug: true
        })
        .factory('basePath', function ($log) {
            var _scripts = angular.element(document).find('script');
            var basePath = '';
            angular.forEach(_scripts, function (v, k) {
                var _src = v.src.split('prova-angular-component.js');
                if (_src.length == 2) {
                    basePath = _src[0];
                }
            });
            $log.info('Trovato base path:', basePath)
            return basePath;
            //      document.getElementsByTagName('script')[2].src.split('prova-angular-component.js')[0]
        });

    // Modules
    angular.module('provaAngularComponent.directives', []);
    angular.module('provaAngularComponent', [
          'provaAngularComponent.config'
          , 'provaAngularComponent.directives'
      ]);

})(angular);

var _app = angular.module('provaAngularComponent.directives', []);
    
_app.directive('welcomeMessage', function(basePath){
    return {
        restrict: 'E',
        templateUrl: basePath + 'views/welcome.html'
      };
});
