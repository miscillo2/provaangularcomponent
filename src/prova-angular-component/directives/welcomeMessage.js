var _app = angular.module('provaAngularComponent.directives', []);
    
_app.directive('welcomeMessage', function(basePath){
    return {
        restrict: 'E',
        templateUrl: basePath + 'views/welcome.html'
      };
});
